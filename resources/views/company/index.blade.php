@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Company</div>

                <div class="card-body">
                   <!-- Button trigger modal -->
<button type="button" class="btn btn-primary mb-2" data-toggle="modal" data-target="#staticBackdrop">
    Create Company
  </button>
  
  <!-- Modal -->
  <div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="staticBackdropLabel">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form action="{{ route('company.store') }}" method="POST" enctype="multipart/form-data">
              @csrf
              @method('POST')
              @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
                <div class="form-group">
                    <label for="">Nama Company</label>
                    <input type="text" name="name" id="" class="form-control">
                </div>
                <div class="form-group">
                    <label for="">email</label>
                    <input type="email" name="email" id="" class="form-control">
                </div>
                <div class="form-group">
                    <label for="">Logo</label>
                    <input type="file" name="logo" id="" class="form-control">
                </div>
                <div class="form-group">
                    <label for="">website</label>
                    <input type="text" name="website" id="" class="form-control">
                </div>
                <button type="submit" class="btn btn-primary">Create</button>
              </form>
        </div>

      </div>
    </div>
  </div>
                    
                    <table class="table">
                        <thead class="thead-dark">
                          <tr>
                            <th scope="col">No</th>
                            <th scope="col">Company </th>
                            <th scope="col">Email</th>
                            <th>Logo</th>
                            <th>Website</th>
                            <th>Aksi</th>
                          </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1;?>
                            @foreach ($company as $com)  
                                               
                            <tr>
                                <th>{{ $no++ }}</th>
                                <th>{{ $com->name }}</th>
                                <th>{{ $com->email }}</th>
                                <th>
                                <img src="{{asset('storage/'.$com->logo)}}" width="100" height="100">
                                </th>
                                <th>{{ $com->website }}</th>
                                <th>
                                  <form action="{{ route('company.destroy', $com->id) }}" method="POST">
                                    @csrf
                                    @method("DELETE")

                                  
                                    <a href="{{ route('company.edit', $com->id) }}"class="btn btn-sm btn-info">Edit</a>
                                    <button type="submit" class="btn btn-sm btn-danger">Hapus</button>
                                  </form> 
                            </tr>
                            @endforeach
                        </tbody>
                      </table>
                     
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
