@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Company</div>

                <div class="card-body">
                 
                    <form action="{{ route('company.update', $company->id) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        @if ($errors->any())
              <div class="alert alert-danger">
                  <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
          @endif
                          <div class="form-group">
                              <label for="">Nama Company</label>
                              <input type="text" name="name" id="" class="form-control" value="{{ $company->name }}">
                          </div>
                          <div class="form-group">
                              <label for="">email</label>
                              <input type="email" name="email" id="" class="form-control" value="{{ $company->email }}">
                          </div>
                          <div class="form-group">
                              <label for="">Logo</label>
                              <input type="file" name="logo" id="" class="form-control">
                          </div>
                          <div class="form-group">
                              <label for="">website</label>
                              <input type="text" name="website" id="" class="form-control" value="{{ $company->website }}">
                          </div>
                          <button type="submit" class="btn btn-primary">Edit</button>
                          <a href="{{ route('company.index') }}" class="btn btn-secondary">Cancel</a>
                        </form>

 
  
 
 
                    
                    
                       
                     
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
