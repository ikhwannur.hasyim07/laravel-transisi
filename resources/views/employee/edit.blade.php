
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Employee</div>

                <div class="card-body">
                    <form action="{{ route('employee.update',$employe->id) }}" method="POST">
                        @csrf
                        @method("PUT")
          
                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                          <div class="form-group">
                              <label for="">Nama</label>
                              <input type="text" name="name" id="" class="form-control" value="{{ $employe->name }}">
                          </div>
                          <div class="form-group">
                              <label for="">email</label>
                              <input type="email" name="email" id="" class="form-control" value="{{ $employe->email }}">
                          </div>
                          <div class="form-group">
                              <label for="">Company</label>
                              <select name="company_id" id="" class="form-control">
                                @foreach ($company as $com)
                                  <option value="{{ $com->id }}">{{ $com->name }}</option>
                                  @endforeach
                              </select>
                          </div>
                          
                          <button type="submit" class="btn btn-primary">Edit</button>
                        </form>    
                      
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
