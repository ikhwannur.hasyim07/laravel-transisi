
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Employee</div>

                <div class="card-body">
<!-- Button trigger modal -->
<button type="button" class="btn btn-primary mb-2" data-toggle="modal" data-target="#staticBackdrop">
   Create Employe
  </button>
  
  <!-- Modal -->
  <div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="staticBackdropLabel">Create Company</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form action="{{ route('employee.store') }}" method="POST">
              @csrf
              @method("POST")

              @if ($errors->any())
              <div class="alert alert-danger">
                  <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
          @endif
                <div class="form-group">
                    <label for="">Nama</label>
                    <input type="text" name="name" id="" class="form-control" >
                </div>
                <div class="form-group">
                    <label for="">email</label>
                    <input type="email" name="email" id="" class="form-control" >
                </div>
                <div class="form-group">
                    <label for="">Company</label>
                    <select name="company_id" id="" class="form-control">
                      @foreach ($company as $com)
                        <option value="{{ $com->id }}">{{ $com->name }}</option>
                        @endforeach
                    </select>
                </div>
                
                <button type="submit" class="btn btn-primary">Create</button>
              </form>
        </div>
        
      </div>
    </div>
  </div>
                    
                    <table class="table">
                        <thead class="thead-dark">
                          <tr>
                            <th scope="col">No</th>
                            <th scope="col">Nama</th>
                            <th scope="col">Company </th>
                            <th scope="col">Email</th>
                            <th>Aksi</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <?php $no =1; ?>
                            @foreach ($employe as $emp)
                            <td>{{ $no++ }}</td>
                            <td>{{ $emp->name }}</td>
                            <td>{{ $emp->company->name }}</td>
                            <td>{{ $emp->email }}</td>
                            <td>
                              <form action="{{ route('employee.destroy', $emp->id) }}" method="POST">
                                @csrf
                                @method("DELETE")
                              <a href="{{ route('employee.edit', $emp->id) }}" class="btn btn-sm btn-info">Edit</a>
                              <button type="submit" class="btn btn-sm btn-danger">Hapus</button>

                            </form>
                            </td>
                            
                          </tr>
                          @endforeach
                        </tbody>
                      </table>
                      
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
